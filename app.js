const express = require('express');
const app = express();
const port = 5000;
const dotenv = require('dotenv')
dotenv.config()

app.get('/', (req, res, next) => {
    res.send('<h1>Hello, World!</h1>');
});

app.listen(port, () => {
    console.log(`Hello, World application listening on port ${port}. Environment variable is: ${process.env.JWT_SECRET}`);
});